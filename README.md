# Simple Web App - Laravel

## Description

Yukk Full Stack Test - Aldi Oktavianto Yahya

### Development Setup

-   Install dependencies

```bash
$ npm install
$ composer install
```

-   env file

```bash
$ cp .env.sample .env
$ php artisan key:generate
```

-   Docker up

```bash
$ npm run start-dev
```

-   Run DB migrate on laravel docker container CLI

```bash
$ php artisan migrate:refresh
```

-   Run local development server in background (open other terminal)

```bash
$ npm run dev
```

-   Visit `localhost` on a browser
