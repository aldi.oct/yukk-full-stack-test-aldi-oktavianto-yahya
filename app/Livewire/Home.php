<?php

namespace App\Livewire;

use App\Models\Transactions;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Home extends Component
{
	use WithPagination;

	public $search;
	public $filterByType;

	public function mount()
	{
		$refererUrl = app('router')->getRoutes()->match(app('request')->create(url()->previous()))->getName();

		if ($refererUrl == 'login' || $refererUrl == 'register')
			session()->flash('success', 'Login Successfull!');
	}

	public function updatedSearch()
	{
		$this->resetPage();
	}

	public function updatedFilterByType()
	{
		$this->resetPage();
	}

	public function render()
	{
		$userId = Auth::user()->id;
		$transactionDB = Transactions::where('users_id', $userId)
			->when(!empty($this->search), function ($query) {
				$query->where('notes', 'LIKE', "%{$this->search}%");
			})

			->when(!empty($this->filterByType), function ($query) {
				$query->where('type', '=', $this->filterByType);
			})

			->orderBy('created_at', 'desc')
			->paginate(5);

		$transactions = [];

		if (count($transactionDB) > 0) {
			foreach ($transactionDB as $transaction) {
				$date = date("d M Y", strtotime($transaction['created_at']));

				$history = [
					'type' => strtolower($transaction['type']),
					'amount' => $transaction['amount'],
					'time' => date("h.i", strtotime($transaction['created_at'])),
					'code' => $transaction['code'],
					'notes' => $transaction['notes'],
				];

				if (!isset($transactions[$date]))
					$transactions[$date] = [];

				array_push($transactions[$date], $history);
			}
		}

		return view("livewire.home", [
			'transactionDB' => $transactionDB,
			'transactionHistory' => $transactions,
		]);
	}
}