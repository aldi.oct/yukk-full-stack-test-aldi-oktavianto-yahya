<?php

namespace App\Livewire;

use App\Models\Transactions;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Transaction extends Component
{
    public $type = "";
    public $amount;
    public $notes;

    public $currentBalance = 0;

    protected $rules = [
        "type" => "required",
        "amount" => "required|numeric|min:1",
        "notes" => "nullable|string|max:255"
    ];

    public function mount()
    {
        $this->currentBalance = User::select('balance')->where('id', Auth::user()->id)->first()->balance;
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function render()
    {
        return view("livewire.transaction");
    }

    public function submit()
    {
        $validatedData = $this->validate();

        try {
            if ($validatedData['type'] === 'TOPUP')
                $this->currentBalance += $validatedData['amount'];
            else {
                if ($this->currentBalance < $validatedData['amount']) {
                    return session()->flash('error', 'Failed to create transaction! Insufficient balance');
                }

                $this->currentBalance -= $validatedData['amount'];
            }

            User::where('id', Auth::user()->id)
                ->update([
                    'balance' => $this->currentBalance,
                ]);

            Auth::user()->balance = $this->currentBalance;

            Transactions::create([
                'users_id' => Auth::user()->id,
                'type' => $validatedData['type'],
                'amount' => $validatedData['amount'],
                'notes' => $validatedData['notes'],
                'code' => 'INV/' . date("Ymd", time()) . '/' . $validatedData['type'] . '/' . sprintf('%010d', rand(1, 1000000))
            ]);

            session()->flash('success', 'Transaction created!');

            return redirect()->route('home');
        } catch (\Exception $e) {
            //block
            dump($e->getMessage());
            $errorMsg = $e->getMessage();
            session()->flash('error', $errorMsg);
        }
    }
}
