@extends('components.layouts.app')

@section('content')

<div class="min-h-screen flex">
    <div class="m-auto min-w-[360px] w-[600px] bg-white flex flex-col gap-y-16 border rounded-xl p-14">
        <h1 class="text-3xl font-bold text-center">
            Login
        </h1>

        <form class="flex flex-col gap-y-10" method="POST" action="{{ route('login') }}">
            @csrf

            <div class="relative">
                <div class="flex gap-x-2.5 border-b pb-2.5">
                    <div class="p-2">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
                        </svg>
                    </div>
                    <input id="email" type="email" class="w-full p-2" placeholder="Type your email" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus />
                </div>
                @error('email')
                <span class="absolute left-[58px] top-[52px] text-red-500 font-bold">{{ $message }}</span>
                @enderror
            </div>
            
            <div class="relative">
                <div class="flex gap-x-2.5 border-b pb-2.5">
                    <div class="p-2">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M16.5 10.5V6.75a4.5 4.5 0 10-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 002.25-2.25v-6.75a2.25 2.25 0 00-2.25-2.25H6.75a2.25 2.25 0 00-2.25 2.25v6.75a2.25 2.25 0 002.25 2.25z" />
                        </svg>
                    </div>
                    <input id="password" type="password" class="w-full p-2" placeholder="Type your password" @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" />
                </div>
                @error('password')
                <span class="absolute left-[58px] top-[52px] text-red-500 font-bold">{{ $message }}</span>
                @enderror
            </div>
              
            <button class="py-2 px-2.5 bg-cyan-500 text-white rounded-xl hover:bg-cyan-600 active:bg-cyan-700 transition ease-in-out duration-150" type="submit">
                Login
            </button>

            <div class="flex gap-x-2">Don't have account?<a href="{{ route('register') }}" class="text-cyan-500">Sign Up</a></div>
        </form>
    </div>
</div>

@endsection