@if (session()->has('error'))
<div id="alert-error-box" class="p-4 w-full text-sm text-red-800 rounded-b-lg bg-gray-800 absolute" role="alert">
    <span class="text-base font-medium">{{ session('error') }}</span>
</div>
<script>
    setTimeout(() => {
    const box = document.getElementById('alert-error-box');
    box.style.display = 'none';
    }, 2000);
</script>
@endif

@if (session()->has('success'))
<div id="alert-success-box" class="p-4 w-full text-sm text-green-800 rounded-b-lg bg-gray-800 absolute" role="alert">
    <span class="text-base font-medium">{{ session('success') }}</span>
</div>
<script>
    setTimeout(() => {
    const box = document.getElementById('alert-success-box');
    box.style.display = 'none';
    }, 2000);
</script>
@endif