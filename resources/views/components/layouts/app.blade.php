<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('components.layouts.head')
    
    <body>
        <main class="bg-gradient-to-r from-cyan-500 to-blue-500">
            @yield('content')

            @isset($slot)
            {{ $slot }}
            @endisset
        </main>

        @livewireScripts
    </body>
</html>
