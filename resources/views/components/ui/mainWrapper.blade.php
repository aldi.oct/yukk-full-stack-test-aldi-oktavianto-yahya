<div class="min-h-screen flex">
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
    <div class="min-h-screen w-[900px] m-auto relative">
        @include('components.common.alert')

        <div class="flex flex-col gap-y-4">
            <div class="flex justify-between items-center p-4 bg-white rounded-b-xl">
                <div class="capitalize font-bold text-lg">{{ 'Hi, ' . Auth::user()->name }}</div>
                <a class="py-2 px-2.5 bg-cyan-500 text-white rounded-xl font-medium hover:bg-cyan-600 active:bg-cyan-700 transition ease-in-out duration-150" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">                    
                    Logout
                </a>
            </div>
    
            <div class="flex items-center p-4 bg-white rounded-xl">
                <div class="flex flex-col gap-y-1 m-auto">
                    <div class="flex items-center gap-x-2 text-lg text-slate-400 self-center">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6">
                            <path d="M21 6.375c0 2.692-4.03 4.875-9 4.875S3 9.067 3 6.375 7.03 1.5 12 1.5s9 2.183 9 4.875z" />
                            <path d="M12 12.75c2.685 0 5.19-.586 7.078-1.609a8.283 8.283 0 001.897-1.384c.016.121.025.244.025.368C21 12.817 16.97 15 12 15s-9-2.183-9-4.875c0-.124.009-.247.025-.368a8.285 8.285 0 001.897 1.384C6.809 12.164 9.315 12.75 12 12.75z" />
                            <path d="M12 16.5c2.685 0 5.19-.586 7.078-1.609a8.282 8.282 0 001.897-1.384c.016.121.025.244.025.368 0 2.692-4.03 4.875-9 4.875s-9-2.183-9-4.875c0-.124.009-.247.025-.368a8.284 8.284 0 001.897 1.384C6.809 15.914 9.315 16.5 12 16.5z" />
                            <path d="M12 20.25c2.685 0 5.19-.586 7.078-1.609a8.282 8.282 0 001.897-1.384c.016.121.025.244.025.368 0 2.692-4.03 4.875-9 4.875s-9-2.183-9-4.875c0-.124.009-.247.025-.368a8.284 8.284 0 001.897 1.384C6.809 19.664 9.315 20.25 12 20.25z" />
                        </svg>                                                 
                        Current Balance
                    </div>
                    <div class="text-cyan-500 font-bold text-3xl text-center">Rp {{ Auth::user()->balance }}</div>
                </div>
            </div>

            @yield('inner-content')
        </div>
    </div>
</div>

