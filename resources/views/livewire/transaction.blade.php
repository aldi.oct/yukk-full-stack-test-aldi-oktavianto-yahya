@extends('components.ui.mainWrapper')

@section('inner-content')

<div class="flex flex-col gap-y-8 p-4 bg-white rounded-xl"> 
    <div class="text-xl font-bold">Create Transaction</div>

    <form class="flex flex-col gap-y-8" wire:submit.prevent="submit">

        <div class="flex flex-col gap-y-2 hidden-arrow-select relative">
            <div class="flex gap-x-4">
                <div class="text-slate-400">Type</div>
                @error('type')
                <span class="text-red-500 font-medium">{{ $message }}</span>
                @enderror
            </div>

            <select wire:model="type" class="p-4 border border-slate-300 rounded-xl">
                <option value="" selected disabled>Select transaction type</option>
                <option value="TOPUP">Topup</option>
                <option value="TRANSACTION">Transaction</option>
            </select>
            <div class="absolute bottom-[18px] right-3.5">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                class="w-4 h-4">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                </svg>
            </div>
        </div>

        <div class="flex flex-col gap-y-2">
            <div class="flex gap-x-4">
                <div class="text-slate-400">Amount</div>
                @error('amount')
                <span class="text-red-500 font-medium">{{ $message }}</span>
                @enderror
            </div>

            <div class="flex border border-slate-300 rounded-xl">
                <div class="font-bold p-4">Rp</div>
                <input wire:model="amount" placeholder="0" class="w-full p-4 border-l border-slate-300 rounded-r-xl" type="text" />
            </div>
        </div>

        <div class="flex flex-col gap-y-2">
            <div class="flex gap-x-4">
                <div class="text-slate-400">Notes <span class="text-sm">(optional)</span></div>
                @error('notes')
                <span class="text-red-500 font-medium">{{ $message }}</span>
                @enderror
            </div>

            <textarea wire:model="notes" class="p-4 border border-slate-300 rounded-xl" cols="30" rows="3"></textarea>
        </div>

        <div class="flex w-full gap-x-2">
            <a class="w-full text-center py-2 px-2.5 bg-slate-400 text-white rounded-xl font-medium hover:bg-slate-500 active:bg-slate-600 transition ease-in-out duration-150" href="{{ route('home') }}">
                Cancel
            </a>
            <button class="w-full text-center gap-x-2 py-2 px-2.5 bg-cyan-500 text-white rounded-xl font-medium hover:bg-cyan-600 active:bg-cyan-700 transition ease-in-out duration-150" type="submit">Submit</button>
        </div>
    </form>
</div>
@endsection