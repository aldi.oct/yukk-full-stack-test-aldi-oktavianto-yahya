@extends('components.ui.mainWrapper')

@section('inner-content')

<div class="flex flex-col gap-y-8 p-4 bg-white rounded-xl">
    <div class="flex flex-col gap-y-4 w-full">
        <div class="flex justify-between">
            <div class="text-xl font-bold">Balance History</div>
            <a class="flex items-center gap-x-2 py-2 px-2.5 bg-cyan-500 text-white rounded-xl font-medium hover:bg-cyan-600 active:bg-cyan-700 transition ease-in-out duration-150" href="{{ route('transaction') }}">                    
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6">
                    <path fill-rule="evenodd" d="M12 3.75a.75.75 0 01.75.75v6.75h6.75a.75.75 0 010 1.5h-6.75v6.75a.75.75 0 01-1.5 0v-6.75H4.5a.75.75 0 010-1.5h6.75V4.5a.75.75 0 01.75-.75z" clip-rule="evenodd" />
                </svg>                              
                Transaction
            </a>
        </div>
        <div class="flex border border-slate-300 rounded-xl">
            <div class="w-1/2">
                <input wire:model.live="search" class="w-full p-4 bg-transparent rounded-l-xl border-r border-slate-300" type="text" placeholder="search"/>
            </div>
            <div class="w-1/2 hidden-arrow-select relative">
                <select wire:model.live="filterByType" class="w-full p-4 bg-transparent rounded-r-xl">
                    <option value="" selected>All</option>
                    <option value="TOPUP">Topup</option>
                    <option value="TRANSACTION">Transaction</option>
                </select>
                <div class="absolute bottom-[18px] right-3.5">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                    </svg>                                  
                </div>
            </div>
        </div>
    </div>

    <div class="flex flex-col gap-y-3 w-full">
        @if (count($transactionHistory) > 0)
        @foreach ($transactionHistory as $key => $transactionItems)
        <div class="flex flex-col gap-y-2">
            <div class="font-bold">{{ $key }}</div>
            @foreach ($transactionItems as $item)
            <div class="flex flex-col gap-y-2">
                <div class="flex gap-x-2 border border-slate-300 rounded-xl p-4">
                    <div class="p-2.5">
                        @switch($item['type'] ?? null)
                            @case('topup')
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-7 h-7">
                                    <path d="M12 7.5a2.25 2.25 0 100 4.5 2.25 2.25 0 000-4.5z" />
                                    <path fill-rule="evenodd" d="M1.5 4.875C1.5 3.839 2.34 3 3.375 3h17.25c1.035 0 1.875.84 1.875 1.875v9.75c0 1.036-.84 1.875-1.875 1.875H3.375A1.875 1.875 0 011.5 14.625v-9.75zM8.25 9.75a3.75 3.75 0 117.5 0 3.75 3.75 0 01-7.5 0zM18.75 9a.75.75 0 00-.75.75v.008c0 .414.336.75.75.75h.008a.75.75 0 00.75-.75V9.75a.75.75 0 00-.75-.75h-.008zM4.5 9.75A.75.75 0 015.25 9h.008a.75.75 0 01.75.75v.008a.75.75 0 01-.75.75H5.25a.75.75 0 01-.75-.75V9.75z" clip-rule="evenodd" />
                                    <path d="M2.25 18a.75.75 0 000 1.5c5.4 0 10.63.722 15.6 2.075 1.19.324 2.4-.558 2.4-1.82V18.75a.75.75 0 00-.75-.75H2.25z" />
                                </svg>
                                @break

                            @default
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-7 h-7">
                                    <path d="M4.5 3.75a3 3 0 00-3 3v.75h21v-.75a3 3 0 00-3-3h-15z" />
                                    <path fill-rule="evenodd" d="M22.5 9.75h-21v7.5a3 3 0 003 3h15a3 3 0 003-3v-7.5zm-18 3.75a.75.75 0 01.75-.75h6a.75.75 0 010 1.5h-6a.75.75 0 01-.75-.75zm.75 2.25a.75.75 0 000 1.5h3a.75.75 0 000-1.5h-3z" clip-rule="evenodd" />
                                </svg>
                        @endswitch
                        
                    </div>
                    <div class="flex justify-between w-full">
                        <div class="flex flex-col">
                            <div class="text-xl capitalize">
                                {{ $item['type'] }}
                                @if ($item['notes'])
                                <span class="text-sm">({{ $item['notes'] }})</span>
                                @endif 
                            </div>
                            <div class="text-sm text-slate-400">{{ $item['code'] }}</div>
                        </div>
                        <div class="flex flex-col text-right">
                            @switch($item['type'] ?? null)
                                @case('topup')
                                    <div class="text-xl text-cyan-500 font-medium">{{ $item['type'] == 'topup' ? '+' : '-' }} Rp {{ $item['amount'] }}</div>
                                    @break

                                @default
                                    <div class="text-xl font-medium">{{ $item['type'] == 'topup' ? '+' : '-' }} Rp {{ $item['amount'] }}</div>
                            @endswitch
                            <div class="text-sm text-slate-400">{{ $item['time'] }} WIB</div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
        {{ $transactionDB->links('components.common.customPagination') }}

        @else
        <div class="font-bold text-xl w-full text-center">No transaction</div>
        @endif
    </div>
</div>
@endsection
